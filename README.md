# Microservice Lumen

Lumen est une version alléger de Laraval. 
Le choix d'utiliser Lumen est de faire une réduction de coup sur l'utilisation de la mémoire et de l'espace disque. 

Cependant si nous avons besoin de laraval nous pourrons faire évoluer Lumen. 

## Environnement 

- PHP 7.4
- Mysql 8
- Lumen 8

## Lancement du container
Pour lancer le container rien de plus simple
``docker-compose up --build``

Si vous souhaitez le lancer en deamon ``docker-compose up -d``

Pour lancer des commandes ouvrez un nouveau terminal s'il n'est pas en deamon, lancer la commande suivante :
``docker exec -it app bash``


